<?php

// Teams list
$teams = [
    'Team 1', 'Team 2', 'Team 3', 'Team 4', 'Team 5', 'Team 6', 'Team 7', 'Team 8', 'Team 9', 'Team 10',
    'Team 11', 'Team 12', 'Team 13', 'Team 14', 'Team 15', 'Team 16', 'Team 17', 'Team 18', 'Team 19'
];

// Rondomize teams order
shuffle($teams);

// Build the championship
function build_champ($teams) {
    $calendar = [];
    $teams_number = count($teams);

    // Ensures the number of teams is even
    if ($teams_number % 2 != 0) {
        array_push($teams, '<strong>Exempte</strong>');
        $teams_number++;
    }

    // For each day
    for ($day = 1; $day < $teams_number; $day++) {
        $calendar[$day] = [];

        // Build all matches of the day
        for ($i = 0; $i < $teams_number / 2; $i++) {
            $team1 = $teams[$i];
            $team2 = $teams[$teams_number - 1 - $i];

            $calendar[$day][] = [$team1, $team2];
        }

        // Rotate teams for next day
        $teamDebut = array_shift($teams);
        array_unshift($teams, array_pop($teams));
        array_unshift($teams, $teamDebut);
    }

    return $calendar;
}

// Create championship
$champ = build_champ($teams);
// Randomize days order
shuffle($champ);

// Display calendar
foreach ($champ as $day => $matches) {
    echo 'Day: ' . $day + 1 . '<br />';
    foreach ($matches as $id => $teams) {
        echo "Match " . $id + 1 . " : " . $teams[0] . " vs " . $teams[1] . "<br />";
    }
    echo "<br>";
}
?>
