
# Petanque casual competition manager

Version 1.1

## Install

Send files on server (php/mysql), join the directory from browser, fill the fields of the install form, play.  
  
On local server : no connection needed  
On online server : Demo/Demo (respect caps)  

## How to

Once it's installed
1. Add members in `Administration` (right top corner)
2. Select members in `Present members`
3. Join `Games manager`
    1. As you selected members, `Games manager` is unlocked
    2. First join in a day will initialize the manager
    3. Add round N°n will create randoms teams then random matches
4. Join `Scoring`
    1. As you initialized the manager then added a first round, `Scoring` is unlocked
    2. Focus the score of loosers then fill it by clicking on a result in the available scores list on top. The score of winners will be auttomaticly filled. Then validate (scores become green)
    3. You can transmit results to the ranking table at anytime (as a first score is validated) by click on `Update Ranking` (will redirect to the ranking page)
5. Ranking rules
    1. Ranking by `victories` desc, then `points for` desc, then `points against` asc, then `member name` asc
6. Monthly ranking (right top corner)
    1. Ranking by victories in a month
    2. Selector to select the wanted month