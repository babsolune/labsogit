<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

define('PATH_TO_ROOT', '..');

require_once PATH_TO_ROOT . '/kernel/init.php';

$url_controller_mappers = [
	// Configuration
	new UrlControllerMapper('AdminDeliveryConfigController', '`^/admin(?:/config)?/?$`'),

	// Items Management
	new UrlControllerMapper('DeliveryItemsManagerController', '`^/manage/?$`'),
	new UrlControllerMapper('DeliveryItemsReorderController', '`^/reorder/?$`'),
	new UrlControllerMapper('DeliveryItemFormController', '`^/add/?$`'),
	new UrlControllerMapper('DeliveryItemFormController', '`^/edit/([0-9]+)/?$`', ['id']),
	new UrlControllerMapper('DeliveryDeletePieceController', '`^/delete/([0-9]+)/?$`', ['id']),

	new UrlControllerMapper('DeliveryPrintController', '`^/print/?$`'),
	new UrlControllerMapper('DeliveryHomeController', '`^/?$`')
];
DispatchManager::dispatch($url_controller_mappers);
?>
