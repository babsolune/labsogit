<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryContent
{
	private static $db_querier;
	private static $config;
	private static $lang;

	public static function __static()
	{
		self::$db_querier = PersistenceContext::get_querier();
        self::$config = DeliveryConfig::load();
        self::$lang = LangLoader::get_all_langs('delivery');
	}

    public static function get_items($type)
    {
        $result = self::$db_querier->select('SELECT * 
        FROM ' . DeliverySetup::$delivery_table . '
		WHERE published = 1
        AND type = "' . $type . '"
        ORDER BY i_order');

        $items = [];
        while ($row = $result->fetch())
        {
            $item = new DeliveryItem();
            $item->set_properties($row);
            $items[] = $item;
        }
        return $items;
    }

    public static function get_card()
    {
        $view = new FileTemplate('delivery/content/DeliveryCardController.tpl');
        $view->add_lang(LangLoader::get_all_langs('delivery'));

        foreach (self::get_items(DeliveryItem::PIECE) as $item)
        {
			$view->assign_block_vars('pieces', $item->get_template_vars());
			foreach ($item->get_pieces() as $id => $piece)
			{
				$view->assign_block_vars('pieces.items', [
                    'ID' => $id,
                    'CODE' => TextHelper::strtoupper($piece['code'] ?? ''),
                    'NAME' => TextHelper::strtoupper($piece['name'] ?? ''),
                    'CONTENT' => self::replace_content('&#8226;', $piece['content']),
                    'PRICE' => $piece['price'],
                ]);
			}
        }

        foreach (self::get_items(DeliveryItem::TRAY) as $item)
        {
			$view->assign_block_vars('trays', $item->get_template_vars());
			foreach ($item->get_pieces() as $id => $piece)
			{
				$view->assign_block_vars('trays.items', [
                    'ID' => $id,
                    'CODE' => TextHelper::strtoupper($piece['code'] ?? ''),
                    'NAME' => TextHelper::strtoupper($piece['name'] ?? ''),
                    'CONTENT' => self::replace_content('<br />', $piece['content']),
                    'PRICE' => $piece['price'],
                ]);
			}
        }
		return $view;
    }

    private static function replace_content(string $replace, string $string)
    {
        $contents = [];
        $array = explode(',', $string);
        foreach ($array as $options)
        {
            if (substr($options, 0, 1) === ' ')
            $options = substr($options, 1);
            $contents[] = TextHelper::ucfirst($options);
        }
        $str = implode(' ' . $replace . ' ', $contents);

        return $str;
    }

    public static function get_menu()
    {
        $view = new FileTemplate('delivery/content/DeliveryStickyMenuController.tpl');
        $view->add_lang(LangLoader::get_all_langs('delivery'));

        foreach (self::get_items(DeliveryItem::PIECE) as $item)
        {
			$view->assign_block_vars('pieces', $item->get_template_vars());
        }

        foreach (self::get_items(DeliveryItem::TRAY) as $item)
        {
			$view->assign_block_vars('trays', $item->get_template_vars());
        }
		return $view;
    }

    public static function get_command()
    {
        $view = new FileTemplate('delivery/content/DeliveryCommandController.tpl');
        $view->add_lang(self::$lang);
        $tel = self::$config->get_phone();
        $link_tel = TextHelper::substr($tel, 1);
        $view->put_all([
            'L_COMMAND_TEXT' => StringVars::replace_vars(self::$lang['delivery.commande.text'], ['link_tel' => $link_tel, 'tel' => $tel])
        ]);

        return $view;
    }

    public static function get_delivery_days()
    {
        $view = new FileTemplate('delivery/content/DeliveryDeliveryController.tpl');
        $view->add_lang(self::$lang);
        $view->put_all([
            'C_MONDAY'    => count(self::$config->get_monday()) > 0,
            'C_TUESDAY'   => count(self::$config->get_tuesday()) > 0,
            'C_WEDNESDAY' => count(self::$config->get_wednesday()) > 0,
            'C_THURSDAY'  => count(self::$config->get_thursday()) > 0,
            'C_FRIDAY'    => count(self::$config->get_friday()) > 0,
            'C_SATURDAY'  => count(self::$config->get_saturday()) > 0,
            'C_SUNDAY'    => count(self::$config->get_sunday()) > 0,
        ]);

        self::get_days(self::$config->get_monday(), 'monday', $view);
        self::get_days(self::$config->get_tuesday(), 'tuesday', $view);
        self::get_days(self::$config->get_wednesday(), 'wednesday', $view);
        self::get_days(self::$config->get_thursday(), 'thursday', $view);
        self::get_days(self::$config->get_friday(), 'friday', $view);
        self::get_days(self::$config->get_saturday(), 'saturday', $view);
        self::get_days(self::$config->get_sunday(), 'sunday', $view);

        return $view;
    }

    private static function get_days(array $array, string $index, $view)
    {
        foreach($array as $options)
        {
            $view->assign_block_vars($index, [
                'TIME'  => $options['hour'] . ':' . $options['minutes'],
                'CITY'  => TextHelper::strtoupper($options['city'] ?? ''),
                'PLACE' => TextHelper::ucfirst($options['place']),
            ]);
        }
        return $view;
    }
}
?>
