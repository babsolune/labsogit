<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryService
{
	private static $db_querier;

	public static function __static()
	{
		self::$db_querier = PersistenceContext::get_querier();
	}

	/** Create a new entry in the database table. */
	public static function add_item(DeliveryItem $item)
	{
		$result = self::$db_querier->insert(DeliverySetup::$delivery_table, $item->get_properties());

		return $result->get_last_inserted_id();
	}

	/** Update an entry. */
	public static function update_item(DeliveryItem $item)
	{
		self::$db_querier->update(DeliverySetup::$delivery_table, $item->get_properties(), 'WHERE id=:id', ['id' => $item->get_id()]);
	}

	/** Delete an entry. */
	public static function delete_item(int $id)
	{
		if (AppContext::get_current_user()->is_readonly())
        {
            $controller = PHPBoostErrors::user_in_read_only();
            DispatchManager::redirect($controller);
        }
			self::$db_querier->delete(DeliverySetup::$delivery_table, 'WHERE id=:id', ['id' => $id]);

			self::$db_querier->delete(DB_TABLE_EVENTS, 'WHERE module=:module AND id_in_module=:id', ['module' => 'delivery', 'id' => $id]);
	}

    public static function update_position($id, $position)
	{
		self::$db_querier->update(DeliverySetup::$delivery_table, array('i_order' => $position), 'WHERE id=:id', array('id' => $id));
	}

	/** Return the item with all its properties from its id. */
	public static function get_piece(int $id)
	{
		$row = self::$db_querier->select_single_row_query('SELECT *
		FROM ' . DeliverySetup::$delivery_table . '
		WHERE id = ' . $id);

		$item = new DeliveryItem();
		$item->set_properties($row);
		return $item;
	}
}
?>
