<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryItem
{
	private $id;
	private $thumbnail_url;
	private $type;
	private $title;
	private $number;
	private $pieces;
	private $published;

    const PIECE  = 'piece';
    const TRAY = 'tray';

    const PENDING = 0;
    const PUBLISHED = 1;

	const THUMBNAIL_URL = '/templates/__default__/images/default_item.webp';

	public function get_id()
	{
		return $this->id;
	}

	public function set_id($id)
	{
		$this->id = $id;
	}

	public function get_type()
	{
		return $this->type;
	}

	public function set_type($type)
	{
		$this->type = $type;
	}

    public function get_type_status()
    {
        switch ($this->type) {
            case DeliveryItem::PIECE :
                return LangLoader::get_message('delivery.pieces.type.piece', 'common', 'delivery');
            break;
            case DeliveryItem::TRAY :
                return LangLoader::get_message('delivery.pieces.type.tray', 'common', 'delivery');
            break;
        }
    }

	public function get_title()
	{
		return $this->title;
	}

	public function set_title($title)
	{
		$this->title = $title;
	}

	public function set_number($number)
	{
		$this->number = $number;
	}

	public function get_number()
	{
		return $this->number;
	}

	public function get_thumbnail()
	{
		if (!$this->thumbnail_url instanceof Url)
			return new Url($this->thumbnail_url == FormFieldThumbnail::DEFAULT_VALUE ? FormFieldThumbnail::get_default_thumbnail_url(self::THUMBNAIL_URL) : $this->thumbnail_url);

		return $this->thumbnail_url;
	}

	public function set_thumbnail($thumbnail)
	{
		$this->thumbnail_url = $thumbnail;
	}

	public function has_thumbnail()
	{
		$thumbnail = ($this->thumbnail_url instanceof Url) ? $this->thumbnail_url->rel() : $this->thumbnail_url;
		return !empty($thumbnail);
	}

	public function add_piece($piece)
	{
		$this->pieces[] = $piece;
	}

	public function set_pieces($pieces)
	{
		$this->pieces = $pieces;
	}

	public function get_pieces()
	{
		return $this->pieces;
	}

	public function is_published()
	{
		return $this->published;
	}

	public function set_published($published)
	{
		$this->published = $published;
	}

    public function get_status()
    {
		switch ($this->published) {
			case self::PUBLISHED:
				return LangLoader::get_message('common.status.published', 'common-lang');
			break;
			case self::PENDING:
				return LangLoader::get_message('common.status.draft', 'common-lang');
			break;
		}
    }

	public function is_authorized_to_add()
	{
		return DeliveryAuthorizationsService::check_authorizations()->write();
	}

	public function is_authorized_to_edit()
	{
		return DeliveryAuthorizationsService::check_authorizations()->write();
	}

	public function is_authorized_to_delete()
	{
		return DeliveryAuthorizationsService::check_authorizations()->write();
	}

	public function get_properties()
	{
		return [
			'id' => $this->get_id(),
			'type' => $this->get_type(),
			'title' => $this->get_title(),
			'published' => $this->is_published(),
			'number' => $this->get_number(),
			'thumbnail' => $this->get_thumbnail()->relative(),
			'pieces' => TextHelper::serialize($this->get_pieces())
        ];
	}

	public function set_properties(array $properties)
	{
		$this->id = $properties['id'];
		$this->type = $properties['type'];
		$this->title = $properties['title'];
		$this->number = $properties['number'];
		$this->published = $properties['published'];
		$this->thumbnail_url = $properties['thumbnail'];
		$this->pieces = !empty($properties['pieces']) ? TextHelper::unserialize($properties['pieces']) : [];
	}

	public function init_default_properties($id_category = Category::ROOT_CATEGORY)
	{
        $this->type = self::PIECE;
        $this->number = 0;
		$this->published = true;
		$this->thumbnail_url = FormFieldThumbnail::DEFAULT_VALUE;
		$this->pieces = [];
	}

	public function get_template_vars()
	{
		$pieces = $this->get_pieces();
		$pieces_nbr = count($pieces);

		return [
            // Conditions
            'C_CONTROLS'      => $this->is_authorized_to_edit() || $this->is_authorized_to_delete(),
            'C_EDIT'          => $this->is_authorized_to_edit(),
            'C_DELETE'        => $this->is_authorized_to_delete(),
            'C_HAS_NUMBER'    => $this->number,
            'C_HAS_THUMBNAIL' => $this->has_thumbnail(),
            'C_PIECES'        => $pieces_nbr > 0,

            // Item
            'ID'                   => $this->id,
            'REWRITED_TITLE' => Url::encode_rewrite($this->title),
            'TITLE'          => TextHelper::ucfirst($this->title),
            'NUMBER'         => $this->number,

            // Links
            'U_EDIT'      => DeliveryUrlBuilder::edit($this->id)->rel(),
            'U_DELETE'    => DeliveryUrlBuilder::delete($this->id)->rel(),
            'U_THUMBNAIL' => $this->get_thumbnail()->rel(),
        ];
	}
}
?>
