<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

####################################################
#                    French                        #
####################################################

$lang['delivery.module.title'] = 'La Carte de Sookie';
$lang['delivery.mini.command'] = 'Commander';
$lang['delivery.mini.delivery'] = 'Livraisons';
$lang['delivery.title.card'] = 'La carte';
$lang['delivery.title.trays'] = 'Les plateaux';
$lang['delivery.title.command'] = 'Commander';
$lang['delivery.title.delivery'] = 'Dates et points de livraison';
$lang['delivery.facebook'] = '@Sookie';

// Pieces
$lang['delivery.piece'] = 'pièce';
$lang['delivery.pieces'] = 'pièces';
$lang['delivery.add.pieces'] = 'Ajouter des pièces';
$lang['delivery.add.trays'] = 'Ajouter un plateau';
$lang['delivery.edit.pieces'] = 'Éditer des pièces';
$lang['delivery.pieces.manager'] = 'Gestion des pièces';
$lang['delivery.pieces.reorder'] = 'Réagencer les pièces';

// Navigation
$lang['delivery.menu']          = 'Navigation';
$lang['delivery.menu.card']     = 'Carte';
$lang['delivery.menu.tray']     = 'Plateaux';
$lang['delivery.menu.tray.xl']  = 'Plateaux XL';
$lang['delivery.menu.command']  = 'Commander';
$lang['delivery.menu.delivery'] = 'Livraisons';

// Commande
$lang['delivery.commande.text'] = '
    <div class="command command-text"><p>Pour vos commandes,<br />appelez-nous au</p></div>
    <div class="command command-tel"><p class="pinned bgc-alt bigger"><a href="tel: +33:link_tel">:tel</a></p><p>Avant <span class="text-strong">17h30</span></p></div>
';

// Form
$lang['delivery.piece.title'] = 'Nom de la pièce';
$lang['delivery.piece.number'] = 'Nombre de pièces';
$lang['delivery.piece.number.clue'] = '0 pour désactiver';
$lang['delivery.piece.details'] = 'Détails des pièces';
$lang['delivery.piece.details.clue'] = 'Séparer les éléments du contenu par des virgules';
$lang['delivery.pieces.type'] = 'Type de pièces';
$lang['delivery.pieces.types'] = 'Types de pièces';
$lang['delivery.pieces.type.piece'] = 'Pièces';
$lang['delivery.pieces.type.tray'] = 'Plateaux';
$lang['delivery.pieces.type.tray.xl'] = 'Plateaux XL';

$lang['delivery.piece.code'] = 'Code';
$lang['delivery.piece.name'] = 'Nom';
$lang['delivery.piece.price'] = 'Prix';
$lang['delivery.piece.content'] = 'Contenu';
$lang['delivery.piece.list'] = 'Liste des pièces';
$lang['delivery.piece.list.clue'] = 'Séparer les pièces par des virgules';
$lang['delivery.published'] = 'Publiée';

// Message Helper
$lang['delivery.message.success.add'] = 'Ajout des pièces :title';
$lang['delivery.message.success.edit'] = 'édition des pièces :title';

// SEO
$lang['delivery.seo.home'] = 'La carte de Sookie Delivery';

// Config #############################################################################################
$lang['delivery.delivery'] = 'Livraisons';
$lang['delivery.monday'] = 'Lundi';
$lang['delivery.tuesday'] = 'Mardi';
$lang['delivery.wednesday'] = 'Mercredi';
$lang['delivery.thursday'] = 'Jeudi';
$lang['delivery.friday'] = 'Vendredi';
$lang['delivery.saturday'] = 'Samedi';
$lang['delivery.sunday'] = 'Dimanche';
$lang['delivery.command'] = 'Commander';
$lang['delivery.phone'] = 'N° de téléphone';
$lang['delivery.day'] = 'Jour';
$lang['delivery.add.day'] = 'Ajouter un jour';
$lang['delivery.edit.day'] = 'Editer un jour';
$lang['delivery.days.manager'] = 'Gestion des tournées';
$lang['delivery.days.reorder'] = 'Réagencer les tournées';
// Message Helper
$lang['delivery.message.success.day.add'] = 'Ajout des pièces :title';
$lang['delivery.message.success.day.edit'] = 'édition des pièces :title';

$lang['delivery.day.city'] = 'Ville';
$lang['delivery.day.place'] = 'Lieu';

// Print
$lang['delivery.see.qrcode'] = 'Voir la Carte';
$lang['delivery.print.card'] = 'La Carte';
$lang['delivery.print.flyer'] = 'Le flyer';
$lang['delivery.print.logo.light'] = 'Logo clair';
$lang['delivery.print.logo.dark'] = 'Logo sombre';
$lang['delivery.print.logo.rounded.light'] = 'Logo rond clair';
$lang['delivery.print.logo.rounded.dark'] = 'Logo rond sombre';
$lang['delivery.print.logo.light'] = 'Logo clair';
$lang['delivery.print.logo.dark'] = 'Logo sombre';
?>
