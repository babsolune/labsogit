<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryHomeController extends DefaultModuleController
{
	protected function get_template_to_use()
	{
		return new FileTemplate('delivery/DeliveryHomeController.tpl');
	}

	public function execute(HTTPRequestCustom $request)
	{
		$this->check_authorizations();

		$this->build_view();

		return $this->generate_response();
	}

	private function build_view()
	{
        $this->view->put_all([
            'MENU' => DeliveryContent::get_menu(),
            'CARD' => DeliveryContent::get_card(),
            'COMMAND' => DeliveryContent::get_command(),
            'DAYS' => DeliveryContent::get_delivery_days()
        ]);
    }

	private function check_authorizations()
	{
		if (!DeliveryAuthorizationsService::check_authorizations()->read())
		{
			$error_controller = PHPBoostErrors::user_not_authorized();
			DispatchManager::redirect($error_controller);
		}
	}

	private function generate_response()
	{
		$response = new SiteDisplayResponse($this->view);
		$graphical_environment = $response->get_graphical_environment();

		$graphical_environment->set_page_title($this->lang['delivery.module.title']);

		$graphical_environment->get_seo_meta_data()->set_description(StringVars::replace_vars($this->lang['delivery.seo.home'], array('site' => GeneralConfig::load()->get_site_name())));
		$graphical_environment->get_seo_meta_data()->set_canonical_url(DeliveryUrlBuilder::home());

		$breadcrumb = $graphical_environment->get_breadcrumb();
		$breadcrumb->add($this->lang['delivery.module.title'], DeliveryUrlBuilder::home());

		return $response;
	}

	public static function get_view()
	{
		$object = new self('delivery');
		$object->check_authorizations();
		$object->build_view(AppContext::get_request());
		return $object->view;
	}
}
?>
