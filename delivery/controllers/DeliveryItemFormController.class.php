<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryItemFormController extends DefaultModuleController
{
	public function execute(HTTPRequestCustom $request)
	{
		$this->check_authorizations();

		$this->build_form($request);

		if ($this->submit_button->has_been_submited() && $this->form->validate())
		{
			$this->save();
			$this->redirect();
		}

		$this->view->put('CONTENT', $this->form->display());

		return $this->generate_response($this->view);
	}

	private function build_form(HTTPRequestCustom $request)
	{
		$item = $this->get_item();

		$form = new HTMLForm(__CLASS__);
		$form->set_layout_title($this->is_new_item ? $this->lang['delivery.add.pieces'] : $this->lang['delivery.edit.pieces']);

		$fieldset = new FormFieldsetHTML('piece', $this->lang['form.parameters']);
		$form->add_fieldset($fieldset);

		$fieldset->add_field( new FormFieldSimpleSelectChoice('type', $this->lang['delivery.pieces.type'], $item->get_type(),
            [
                new FormFieldSelectChoiceOption($this->lang['delivery.pieces.type.piece'], DeliveryItem::PIECE),
                new FormFieldSelectChoiceOption($this->lang['delivery.pieces.type.tray'], DeliveryItem::TRAY)
            ]
        ));

		$fieldset->add_field(new FormFieldTextEditor('title', $this->lang['delivery.piece.title'], $item->get_title(),
			['required' => true]
		));

		$fieldset->add_field(new FormFieldNumberEditor('number', $this->lang['delivery.piece.number'], $item->get_number(),
            [
                'min' => 0,
                'description' => $this->lang['delivery.piece.number.clue']
            ]
        ));

        $fieldset->add_field(new DeliveryFormFieldPiece('pieces', $this->lang['delivery.piece.details'], $item->get_pieces(),
            [
                'class' => 'pieces-details',
                'description' => $this->lang['delivery.piece.details.clue']
            ]
        ));

		$fieldset->add_field(new FormFieldThumbnail('thumbnail', $this->lang['form.picture'], $item->get_thumbnail()->relative(), DeliveryItem::THUMBNAIL_URL));

		$fieldset->add_field(new FormFieldCheckbox('published', $this->lang['delivery.published'], $item->is_published()));

		$fieldset->add_field(new FormFieldHidden('referrer', $request->get_url_referrer()));

		$this->submit_button = new FormButtonDefaultSubmit();
		$form->add_button($this->submit_button);
		$form->add_button(new FormButtonReset());

		$this->form = $form;
	}

    private function save()
    {
        $item = $this->get_item();

        $item->set_type($this->form->get_value('type')->get_raw_value());
        $item->set_title($this->form->get_value('title'));
        $item->set_number($this->form->get_value('number'));
        $item->set_thumbnail($this->form->get_value('thumbnail'));
        $item->set_pieces($this->form->get_value('pieces'));
        $item->set_published($this->form->get_value('published'));

        if ($this->is_new_item)
        {
            $id = DeliveryService::add_item($item);
            $item->set_id($id);
        }
        else
            DeliveryService::update_item($item);
    }

	private function get_item()
	{
		if ($this->item === null)
		{
			$request = AppContext::get_request();
			$id = $request->get_getint('id', 0);
			if (!empty($id))
			{
				try {
					$this->item = DeliveryService::get_piece($id);
				} catch (RowNotFoundException $e) {
					$error_controller = PHPBoostErrors::unexisting_page();
					DispatchManager::redirect($error_controller);
				}
			}
			else
			{
				$this->is_new_item = true;
				$this->item = new DeliveryItem();
				$this->item->init_default_properties();
			}
		}
		return $this->item;
	}

	private function check_authorizations()
	{
		$item = $this->get_item();

		if ($this->is_new_item && !$item->is_authorized_to_add())
		{
            $error_controller = PHPBoostErrors::user_not_authorized();
            DispatchManager::redirect($error_controller);
		}
		elseif (!$item->is_authorized_to_edit())
        {
            $error_controller = PHPBoostErrors::user_not_authorized();
            DispatchManager::redirect($error_controller);
        }
		if (AppContext::get_current_user()->is_readonly())
		{
			$error_controller = PHPBoostErrors::user_in_read_only();
			DispatchManager::redirect($error_controller);
		}
	}

	private function redirect()
	{
		$item = $this->get_item();
		AppContext::get_response()->redirect(DeliveryUrlBuilder::manage(), StringVars::replace_vars($this->is_new_item ? $this->lang['delivery.message.success.add'] : $this->lang['delivery.message.success.edit'], array('title' => $item->get_title())));
	}

	private function generate_response(View $view)
	{
		$item = $this->get_item();

		$location_id = $item->get_id() ? 'delivery-edit-' . $item->get_id() : '';

		$response = new SiteDisplayResponse($view, $location_id);
		$graphical_environment = $response->get_graphical_environment();

		$breadcrumb = $graphical_environment->get_breadcrumb();
		$breadcrumb->add($this->lang['delivery.module.title'], DeliveryUrlBuilder::home());

		if ($this->is_new_item)
		{
			$graphical_environment->set_page_title($this->lang['delivery.add.pieces'], $this->lang['delivery.module.title']);
			$graphical_environment->get_seo_meta_data()->set_description($this->lang['delivery.add.pieces']);
			$graphical_environment->get_seo_meta_data()->set_canonical_url(DeliveryUrlBuilder::add());
			$breadcrumb->add($this->lang['delivery.add.pieces'], DeliveryUrlBuilder::add());
		}
		else
		{
			if (!AppContext::get_session()->location_id_already_exists($location_id))
				$graphical_environment->set_location_id($location_id);

            $page_title = $this->lang['delivery.edit.pieces'];
            $page_url = DeliveryUrlBuilder::edit($item->get_id());
			$graphical_environment->set_page_title($page_title . ': ' . $item->get_title(), $this->lang['delivery.module.title']);
			$graphical_environment->get_seo_meta_data()->set_description($page_title);
			$graphical_environment->get_seo_meta_data()->set_canonical_url($page_url);

			$breadcrumb->add($item->get_title(), DeliveryUrlBuilder::manage());

			$breadcrumb->add($page_title, $page_url);
		}

		return $response;
	}
}
?>
