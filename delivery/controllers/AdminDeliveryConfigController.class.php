<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class AdminDeliveryConfigController extends DefaultAdminModuleController
{
	public function execute(HTTPRequestCustom $request)
	{
		$this->build_form();

		$this->view = new StringTemplate('# INCLUDE MESSAGE_HELPER # # INCLUDE CONTENT #');
		$this->view->add_lang($this->lang);

		if ($this->submit_button->has_been_submited() && $this->form->validate())
		{
			$this->save();
			$this->view->put('MESSAGE_HELPER', MessageHelper::display($this->lang['warning.success.config'], MessageHelper::SUCCESS, 5));
		}

		$this->view->put('CONTENT', $this->form->display());

		return new DefaultAdminDisplayResponse($this->view);
	}

	private function build_form()
	{
		$form = new HTMLForm(__CLASS__);

		$fieldset = new FormFieldsetHTML('configuration', StringVars::replace_vars($this->lang['form.module.title'], array('module_name' => self::get_module()->get_configuration()->get_name())));
		$form->add_fieldset($fieldset);

		$delivery_fieldset = new FormFieldsetHTML('delivery', $this->lang['delivery.delivery']);
		$form->add_fieldset($delivery_fieldset);

		$delivery_fieldset->add_field(new DeliveryFormFieldDay('monday', $this->lang['delivery.monday'], $this->config->get_monday(), ['class' => 'top-field']));
		$delivery_fieldset->add_field(new DeliveryFormFieldDay('tuesday', $this->lang['delivery.tuesday'], $this->config->get_tuesday(), ['class' => 'top-field']));
		$delivery_fieldset->add_field(new DeliveryFormFieldDay('wednesday', $this->lang['delivery.wednesday'], $this->config->get_wednesday(), ['class' => 'top-field']));
		$delivery_fieldset->add_field(new DeliveryFormFieldDay('thursday', $this->lang['delivery.thursday'], $this->config->get_thursday(), ['class' => 'top-field']));
		$delivery_fieldset->add_field(new DeliveryFormFieldDay('friday', $this->lang['delivery.friday'], $this->config->get_friday(), ['class' => 'top-field']));
		$delivery_fieldset->add_field(new DeliveryFormFieldDay('saturday', $this->lang['delivery.saturday'], $this->config->get_saturday(), ['class' => 'top-field']));
		$delivery_fieldset->add_field(new DeliveryFormFieldDay('sunday', $this->lang['delivery.sunday'], $this->config->get_sunday(), ['class' => 'top-field']));

		$command_fieldset = new FormFieldsetHTML('command_config', $this->lang['delivery.command']);
		$form->add_fieldset($command_fieldset);

		$command_fieldset->add_field(new FormFieldTelEditor('phone', $this->lang['delivery.phone'], $this->config->get_phone()));

		$fieldset_authorizations = new FormFieldsetHTML('authorizations_fieldset', $this->lang['form.authorizations'], 
			array('description' => $this->lang['form.authorizations.clue']) 
		);
		$form->add_fieldset($fieldset_authorizations);

		$auth_settings = new AuthorizationsSettings([
            new ActionAuthorization($this->lang['form.authorizations.read'], DeliveryAuthorizationsService::READ_AUTHORIZATIONS),
            new MemberDisabledActionAuthorization($this->lang['form.authorizations.write'], DeliveryAuthorizationsService::WRITE_AUTHORIZATIONS)
		]);
		$auth_settings->build_from_auth_array($this->config->get_authorizations());
		$fieldset_authorizations->add_field(new FormFieldAuthorizationsSetter('authorizations', $auth_settings));

		$this->submit_button = new FormButtonDefaultSubmit();
		$form->add_button($this->submit_button);
		$form->add_button(new FormButtonReset());

		$this->form = $form;
	}

	private function save()
	{
		$this->config->set_monday($this->form->get_value('monday'));
		$this->config->set_tuesday($this->form->get_value('tuesday'));
		$this->config->set_wednesday($this->form->get_value('wednesday'));
		$this->config->set_thursday($this->form->get_value('thursday'));
		$this->config->set_friday($this->form->get_value('friday'));
		$this->config->set_saturday($this->form->get_value('saturday'));
		$this->config->set_sunday($this->form->get_value('sunday'));

        $this->config->set_phone($this->form->get_value('phone'));
		$this->config->set_authorizations($this->form->get_value('authorizations')->build_auth_array());
		DeliveryConfig::save();
    }
}
?>
