<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryItemsReorderController extends DefaultModuleController
{
	protected function get_template_to_use()
	{
		return new FileTemplate('delivery/DeliveryItemsReorderController.tpl');
	}

	public function execute(HTTPRequestCustom $request)
	{
		$this->check_authorizations();

		if ($request->get_value('submit', false))
		{
			$this->update_position($request);
			AppContext::get_response()->redirect(DeliveryUrlBuilder::home(), $this->lang['warning.success.position.update']);
		}

		$this->build_view($request);

		return $this->generate_response();
	}

	private function build_view(HTTPRequestCustom $request)
	{
		$result = PersistenceContext::get_querier()->select('SELECT *
		FROM '. DeliverySetup::$delivery_table .'
		WHERE published = 1
		ORDER BY i_order ASC');

        $this->view->put_all([
			'C_ITEMS'                => $result->get_rows_count() > 0,
			'C_SEVERAL_ITEMS'        => $result->get_rows_count() > 1,
			'ITEMS_NUMBER'         => $result->get_rows_count(),
        ]);

        while ($row = $result->fetch())
		{
			$item = new DeliveryItem();
			$item->set_properties($row);

			$this->view->assign_block_vars('items', $item->get_template_vars());
		}
		$result->dispose();
	}

	private function check_authorizations()
	{
		if (!DeliveryAuthorizationsService::check_authorizations()->write())
		{
			$error_controller = PHPBoostErrors::user_not_authorized();
			DispatchManager::redirect($error_controller);
		}
	}

	private function update_position(HTTPRequestCustom $request)
	{
		$list = json_decode(TextHelper::html_entity_decode($request->get_value('tree')));
		foreach($list as $position => $tree)
		{
			DeliveryService::update_position($tree->id, $position);
		}
	}

	private function generate_response()
	{
		$response = new SiteDisplayResponse($this->view);
		$graphical_environment = $response->get_graphical_environment();

		$graphical_environment->set_page_title($this->lang['delivery.module.title']);

		$graphical_environment->get_seo_meta_data()->set_description(StringVars::replace_vars($this->lang['delivery.seo.home'], array('site' => GeneralConfig::load()->get_site_name())));
		$graphical_environment->get_seo_meta_data()->set_canonical_url(DeliveryUrlBuilder::home());

		$breadcrumb = $graphical_environment->get_breadcrumb();
		$breadcrumb->add($this->lang['delivery.module.title'], DeliveryUrlBuilder::home());
		$breadcrumb->add($this->lang['delivery.pieces.reorder'], DeliveryUrlBuilder::reorder());

		return $response;
	}
}
?>
