<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryPrintController extends DefaultModuleController
{
	public function execute(HTTPRequestCustom $request)
	{
		$this->check_authorizations();

		$this->build_view();

		return new SiteNodisplayResponse($this->view);
	}

	private function build_view()
	{
		$this->view->put_all([
            'PAGE_TITLE' => $this->lang['common.printable'] . ' - ' . $this->lang['delivery.module.title'],
            'TITLE' => $this->lang['delivery.module.title'],
            'CARD' => DeliveryContent::get_card(),
            'COMMAND' => DeliveryContent::get_command()
        ]);
	}

	protected function get_template_to_use()
	{
		return new FileTemplate('delivery/DeliveryPrintController.tpl');
	}

	protected function check_authorizations()
	{
		if (!DeliveryAuthorizationsService::check_authorizations()->read())
		{
			$error_controller = PHPBoostErrors::user_not_authorized();
			DispatchManager::redirect($error_controller);
		}
	}
}
?>
