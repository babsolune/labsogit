<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryItemsManagerController extends DefaultModuleController
{
	private $elements_number = 0;
	private $ids = [];

	public function execute(HTTPRequestCustom $request)
	{
		$this->check_authorizations();

		$current_page = $this->build_table();

		$this->execute_multiple_delete_if_needed($request);

		return $this->generate_response($current_page);
	}

	private function build_table()
	{
		$columns = [
			new HTMLTableColumn($this->lang['common.type'], 'type'),
			new HTMLTableColumn($this->lang['common.name'], 'title'),
			new HTMLTableColumn($this->lang['delivery.piece.number'], 'number'),
			new HTMLTableColumn($this->lang['delivery.piece.details'], 'pieces'),
			new HTMLTableColumn($this->lang['common.status'], 'published'),
			new HTMLTableColumn($this->lang['common.actions'], '', ['sr-only' => true])
        ];

		$table_model = new SQLHTMLTableModel(DeliverySetup::$delivery_table, 'pieces-manager', $columns, new HTMLTableSortingRule('i_order', HTMLTableSortingRule::ASC));

		$table_model->set_layout_title($this->lang['delivery.pieces.manager']);

		$table = new HTMLTable($table_model);

		$results = [];
		$result = $table_model->get_sql_results('delivery',
			['*', 'delivery.id']
		);
		foreach ($result as $row)
		{
			$item = new DeliveryItem();
			$item->set_properties($row);

			$this->elements_number++;
			$this->ids[$this->elements_number] = $item->get_id();

            $pieces_details = [];
            foreach ($item->get_pieces() as $piece)
            {
                $code = $piece['code'] ?? '';
                $name = $piece['name'] ?? '';
                $pieces_details[] = $code . ' ' . $name;
            }
            $pieces_nb = count($pieces_details);
            $pieces_nb = $pieces_nb . ' ' . ($pieces_nb > 1 ? $this->lang['delivery.pieces.types'] : $this->lang['delivery.pieces.type']);
            $pieces_label = implode('<br />', $pieces_details);
            $pieces = '<span class="" aria-label="' . $pieces_label . '">' . $pieces_nb . '</span>';

			$edit_link = new EditLinkHTMLElement(DeliveryUrlBuilder::edit($item->get_id()));
			$delete_link = new DeleteLinkHTMLElement(DeliveryUrlBuilder::delete($item->get_id()));

			$row = [
				new HTMLTableRowCell($item->get_type_status(), 'align-left'),
				new HTMLTableRowCell($item->get_title(), 'align-center'),
				new HTMLTableRowCell($item->get_number() ? $item->get_number() : '', 'align-center'),
				new HTMLTableRowCell($pieces, 'align-center'),
				new HTMLTableRowCell($item->get_status()),
				new HTMLTableRowCell($edit_link->display() . $delete_link->display(), 'controls')
            ];

			$results[] = new HTMLTableRow($row);
		}
		$table->set_rows($table_model->get_number_of_matching_rows(), $results);

		$this->view->put('CONTENT', $table->display());

		return $table->get_page_number();
	}

	private function execute_multiple_delete_if_needed(HTTPRequestCustom $request)
    {
        if ($request->get_string('delete-selected-elements', false))
        {
            for ($i = 1; $i <= $this->elements_number; $i++)
            {
                if ($request->get_value('delete-checkbox-' . $i, 'off') == 'on')
                {
                    if (isset($this->ids[$i]))
                    {
						$item = DeliveryService::get_piece($this->ids[$i]);
                        DeliveryService::delete_item($this->ids[$i]);
						HooksService::execute_hook_action('delete', self::$module_id, $item->get_properties());
                    }
                }
            }

            AppContext::get_response()->redirect(DeliveryUrlBuilder::manage(), $this->lang['warning.process.success']);
        }
    }

	private function check_authorizations()
	{
		if (!DeliveryAuthorizationsService::check_authorizations()->write())
		{
			$error_controller = PHPBoostErrors::user_not_authorized();
			DispatchManager::redirect($error_controller);
		}
	}

	private function generate_response($page = 1)
	{
		$response = new SiteDisplayResponse($this->view);

		$graphical_environment = $response->get_graphical_environment();
		$graphical_environment->set_page_title($this->lang['delivery.pieces.manager'], $this->lang['delivery.module.title'], $page);
		$graphical_environment->get_seo_meta_data()->set_canonical_url(DeliveryUrlBuilder::manage());

		$breadcrumb = $graphical_environment->get_breadcrumb();
		$breadcrumb->add($this->lang['delivery.module.title'], DeliveryUrlBuilder::home());

		$breadcrumb->add($this->lang['delivery.pieces.manager'], DeliveryUrlBuilder::manage());

		return $response;
	}
}
?>
