<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryUrlBuilder
{
	private static $dispatcher = '/delivery';

	public static function configuration():Url
	{
		return DispatchManager::get_url(self::$dispatcher, '/admin/config');
	}

    // Pieces
	public static function manage():Url
	{
		return DispatchManager::get_url(self::$dispatcher, '/manage/');
	}

	public static function reorder():Url
	{
		return DispatchManager::get_url(self::$dispatcher, '/reorder/');
	}

	public static function print():Url
	{
		return DispatchManager::get_url(self::$dispatcher, '/print/');
	}

	public static function add():Url
	{
		return DispatchManager::get_url(self::$dispatcher, '/add/');
	}

	public static function edit($id):Url
	{
		return DispatchManager::get_url(self::$dispatcher, '/edit/' . $id);
	}

	public static function delete($id):Url
	{
		return DispatchManager::get_url(self::$dispatcher, '/delete/' . $id . '/' . '?token=' . AppContext::get_session()->get_token());
	}

	public static function home():Url
	{
		return DispatchManager::get_url(self::$dispatcher, '/');
	}
}
?>
