<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryMiniDelivery extends ModuleMiniMenu
{
    private $view;
	public function get_default_block()
	{
		return self::BLOCK_POSITION__LEFT;
	}

	public function admin_display()
	{
		return '';
	}

	public function get_menu_id()
	{
		return 'module-mini-delivery';
	}

	public function get_menu_title()
	{
		return LangLoader::get_message('delivery.mini.delivery', 'common', 'delivery');
	}

	public function is_displayed()
	{
		return DeliveryAuthorizationsService::check_authorizations()->read();
	}

	public function get_menu_content()
	{
		$lang = LangLoader::get_all_langs('delivery');
		$this->view = new FileTemplate('delivery/DeliveryMiniDelivery.tpl');
		$this->view->add_lang($lang);
		MenuService::assign_positions_conditions($this->view, $this->get_block());
		Menu::assign_common_template_variables($this->view);

		$this->view->put('DAYS', DeliveryContent::get_delivery_days());
		return $this->view->render();
    }

	public function display()
	{
		if ($this->is_displayed())
		{
            return $this->get_menu_content();
		}
	}
}
?>
