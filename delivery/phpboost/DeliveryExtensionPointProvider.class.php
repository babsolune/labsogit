<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryExtensionPointProvider extends ExtensionPointProvider
{
    public function __construct()
    {
        parent::__construct('delivery');
    }

    public function home_page()
	{
		return new DefaultHomePageDisplay($this->get_id(), DeliveryHomeController::get_view());
	}

	public function css_files()
	{
		$module_css_files = new ModuleCssFiles();
		$module_css_files->adding_running_module_displayed_file('delivery.css');
		$module_css_files->adding_always_displayed_file('delivery_mini.css');
		return $module_css_files;
	}

    public function menus()
    {
        return new ModuleMenus([
            new DeliveryMiniCommand(),
            new DeliveryMiniDelivery()
        ]);
    }

    public function tree_links()
	{
		return new DeliveryTreeLinks();
	}

	public function url_mappings()
	{
		return new UrlMappings(array(new DispatcherUrlMapping('/delivery/index.php')));
	}
}
?>
