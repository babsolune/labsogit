<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryMiniCommand extends ModuleMiniMenu
{
	public function get_default_block()
	{
		return self::BLOCK_POSITION__LEFT;
	}

	public function admin_display()
	{
		return '';
	}

	public function get_menu_id()
	{
		return 'module-mini-delivery-delivery';
	}

	public function get_menu_title()
	{
		return LangLoader::get_message('delivery.mini.command', 'common', 'delivery');
	}

	public function is_displayed()
	{
		return DeliveryAuthorizationsService::check_authorizations()->read();
	}

	public function get_menu_content()
	{
		$lang = LangLoader::get_all_langs('delivery');
		$view = new FileTemplate('delivery/DeliveryMiniCommand.tpl');
		$view->add_lang($lang);
		MenuService::assign_positions_conditions($view, $this->get_block());
		Menu::assign_common_template_variables($view);

		$view->put('COMMAND', DeliveryContent::get_command());

		return $view->render();
	}

	public function display()
	{
		if ($this->is_displayed())
		{
            return $this->get_menu_content();
		}
	}
}
?>
