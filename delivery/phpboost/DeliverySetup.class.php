<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliverySetup extends DefaultModuleSetup
{
	public static $delivery_table;
	public static $delivery_day_table;

	public static function __static()
	{
		self::$delivery_table = PREFIX . 'delivery';
		self::$delivery_day_table = PREFIX . 'delivery_day';
	}

	public function install()
	{
		$this->drop_tables();
		$this->create_tables();
	}

	public function uninstall()
	{
		$this->drop_tables();
		ConfigManager::delete('delivery', 'config');
		CacheManager::invalidate('module', 'delivery');
	}

	private function drop_tables()
	{
		PersistenceContext::get_dbms_utils()->drop([
            self::$delivery_table,
            self::$delivery_day_table,
        ]);
	}

	private function create_tables()
	{
		$this->create_delivery_table();
		$this->create_delivery_day_table();
	}

	private function create_delivery_table()
	{
		$fields = array(
			'id' => array('type' => 'integer', 'length' => 11, 'autoincrement' => true, 'notnull' => 1),
			'i_order' => array('type' => 'integer', 'length' => 11, 'notnull' => 1, 'default' => 0),
			'thumbnail' => array('type' => 'string', 'length' => 255, 'notnull' => 1, 'default' => "''"),
			'published' => array('type' => 'integer', 'length' => 1, 'notnull' => 1, 'default' => 0),
            'type' => array('type' => 'string', 'length' => 255, 'notnull' => 1, 'default' => "''"),
			'title' => array('type' => 'string', 'length' => 255),
			'number' => array('type' => 'integer', 'length' => 11),
			'pieces' => array('type' => 'text', 'length' => 65000)
		);
		$options = array(
			'primary' => array('id'),
			'indexes' => array(
				'title' => array('type' => 'fulltext', 'fields' => 'title')
			)
		);
		PersistenceContext::get_dbms_utils()->create_table(self::$delivery_table, $fields, $options);
	}

	private function create_delivery_day_table()
	{
		$fields = array(
			'id' => array('type' => 'integer', 'length' => 11, 'autoincrement' => true, 'notnull' => 1),
			'published' => array('type' => 'integer', 'length' => 1, 'notnull' => 1, 'default' => 0),
			'day' => array('type' => 'string', 'length' => 255, 'notnull' => 1, 'default' => "''"),
			'locations' => array('type' => 'text', 'length' => 65000)
		);
		$options = array(
			'primary' => array('id'),
			'indexes' => array(
				'day' => array('type' => 'fulltext', 'fields' => 'day')
			)
		);
		PersistenceContext::get_dbms_utils()->create_table(self::$delivery_day_table, $fields, $options);
	}
}
?>
