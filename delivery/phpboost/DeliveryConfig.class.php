<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryConfig extends AbstractConfigData
{
	const MONDAY    = 'monday';
	const TUESDAY   = 'tuesday';
	const WEDNESDAY = 'wednasday';
	const THURSDAY  = 'thursday';
	const FRIDAY    = 'friday';
	const SATURDAY  = 'saturday';
	const SUNDAY    = 'sunday';

    const PHONE = 'phone';
	const AUTHORIZATIONS = 'authorizations';

	public function get_monday()
	{
		return $this->get_property(self::MONDAY);
	}

	public function set_monday(Array $monday)
	{
		$this->set_property(self::MONDAY, $monday);
	}

	public function get_tuesday()
	{
		return $this->get_property(self::TUESDAY);
	}

	public function set_tuesday(Array $tuesday)
	{
		$this->set_property(self::TUESDAY, $tuesday);
	}

	public function get_wednesday()
	{
		return $this->get_property(self::WEDNESDAY);
	}

	public function set_wednesday(Array $wednesday)
	{
		$this->set_property(self::WEDNESDAY, $wednesday);
	}

	public function get_thursday()
	{
		return $this->get_property(self::THURSDAY);
	}

	public function set_thursday(Array $thursday)
	{
		$this->set_property(self::THURSDAY, $thursday);
	}

	public function get_friday()
	{
		return $this->get_property(self::FRIDAY);
	}

	public function set_friday(Array $friday)
	{
		$this->set_property(self::FRIDAY, $friday);
	}

	public function get_saturday()
	{
		return $this->get_property(self::SATURDAY);
	}

	public function set_saturday(Array $saturday)
	{
		$this->set_property(self::SATURDAY, $saturday);
	}

	public function get_sunday()
	{
		return $this->get_property(self::SUNDAY);
	}

	public function set_sunday(Array $sunday)
	{
		$this->set_property(self::SUNDAY, $sunday);
	}

	public function get_phone()
	{
		return $this->get_property(self::PHONE);
	}

	public function set_phone($phone)
	{
		$this->set_property(self::PHONE, $phone);
	}

	public function get_authorizations()
	{
		return $this->get_property(self::AUTHORIZATIONS);
	}

	public function set_authorizations(Array $authorizations)
	{
		$this->set_property(self::AUTHORIZATIONS, $authorizations);
	}

	public function get_default_values()
	{
		return [
            self::MONDAY => [],
            self::TUESDAY => [],
            self::WEDNESDAY => [],
            self::THURSDAY => [],
            self::FRIDAY => [],
            self::SATURDAY => [],
            self::SUNDAY => [],
            self::PHONE => '0123456789',
			self::AUTHORIZATIONS => ['r-1' => 65, 'r0' => 69, 'r1' => 85]
        ];
	}

	/**
	 * Returns the configuration.
	 * @return DeliveryConfig
	 */
	public static function load()
	{
		return ConfigManager::load(__CLASS__, 'delivery', 'config');
	}

	/**
	 * Saves the configuration in the database. Has it become persistent.
	 */
	public static function save()
	{
		ConfigManager::save('delivery', self::load(), 'config');
	}
}
?>
