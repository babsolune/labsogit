<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryTreeLinks implements ModuleTreeLinksExtensionPoint
{
	public function get_actions_tree_links()
	{
		$lang = LangLoader::get_all_langs('delivery');
		$tree = new ModuleTreeLinks();
		$tree->add_link(new AdminModuleLink($lang['form.configuration'], DeliveryUrlBuilder::configuration()));

		$tree->add_link(new ModuleLink($lang['delivery.add.pieces'], DeliveryUrlBuilder::add(), DeliveryAuthorizationsService::check_authorizations()->write()));
		$tree->add_link(new ModuleLink($lang['delivery.pieces.manager'], DeliveryUrlBuilder::manage(), DeliveryAuthorizationsService::check_authorizations()->write()));
		$tree->add_link(new ModuleLink($lang['delivery.pieces.reorder'], DeliveryUrlBuilder::reorder(), DeliveryAuthorizationsService::check_authorizations()->write()));
		$tree->add_link(new ModuleLink($lang['common.printable'], DeliveryUrlBuilder::print(), DeliveryAuthorizationsService::check_authorizations()->write()));

		if (ModulesManager::get_module('delivery')->get_configuration()->get_documentation())
			$tree->add_link(new ModuleLink($lang['form.documentation'], ModulesManager::get_module('delivery')->get_configuration()->get_documentation(), DeliveryAuthorizationsService::check_authorizations()->write()));

		return $tree;
	}
}
?>
