# IF C_ITEMS #
	<script>
		var DeliveryItems = function(id){
			this.id = id;
			this.items_number = {ITEMS_NUMBER};
		};

		DeliveryItems.prototype = {
			init_sortable : function() {
				jQuery("ul#items-list").sortable({
					handle: '.sortable-selector',
					placeholder: '<div class="dropzone">' + ${escapejs(@common.drop.here)} + '</div>',
					onDrop: function ($item, container, _super, event) {
						DeliveryItems.change_reposition_pictures();
						$item.removeClass(container.group.options.draggedClass).removeAttr("style");
						$("body").removeClass(container.group.options.bodyClass);
					}
				});
			},
			serialize_sortable : function() {
				jQuery('#tree').val(JSON.stringify(this.get_sortable_sequence()));
			},
			get_sortable_sequence : function() {
				var sequence = jQuery("ul#items-list").sortable("serialize").get();
				return sequence[0];
			},
			change_reposition_pictures : function() {
				sequence = this.get_sortable_sequence();
				var length = sequence.length;
				for(var i = 0; i < length; i++)
				{
					if (jQuery('#list-' + sequence[i].id).is(':first-child'))
						jQuery("#move-up-" + sequence[i].id).hide();
					else
						jQuery("#move-up-" + sequence[i].id).show();

					if (jQuery('#list-' + sequence[i].id).is(':last-child'))
						jQuery("#move-down-" + sequence[i].id).hide();
					else
						jQuery("#move-down-" + sequence[i].id).show();
				}
			}
		};

		var FaqItem = function(id, delivery_items){
			this.id = id;
			this.DeliveryItems = delivery_items;

			if (DeliveryItems.items_number > 1)
				DeliveryItems.change_reposition_pictures();
		};

		var DeliveryItems = new DeliveryItems('items-list');
		jQuery(document).ready(function() {
			DeliveryItems.init_sortable();
		});
	</script>
# ENDIF #
# INCLUDE MESSAGE_HELPER #
<section id="module-delivery">
	<header class="section-header">
		<h1>
			{@delivery.pieces.reorder}
		</h1>
	</header>

	<div class="sub-section">
		<div class="content-container">
			<div class="content">
				# IF C_ITEMS #
					<form action="{REWRITED_SCRIPT}" method="post" id="position-update-form" onsubmit="DeliveryItems.serialize_sortable();" class="delivery-reorder-form">
						<fieldset id="items-management">
							<ul id="items-list" class="sortable-block">
								# START items #
									<li class="sortable-element# IF items.C_NEW_CONTENT # new-content# ENDIF #" id="list-{items.ID}" data-id="{items.ID}">
										<div class="sortable-selector" aria-label="{@common.move}"></div>
										<div class="sortable-title">
											<span class="item-title">{items.TITLE}</span>
										</div>
										<div class="sortable-actions">
											# IF C_SEVERAL_ITEMS #
												<a href="#" aria-label="{@common.move.up}" id="move-up-{items.ID}" onclick="return false;"><i class="fa fa-fw fa-arrow-up" aria-hidden="true"></i></a>
												<a href="#" aria-label="{@common.move.down}" id="move-down-{items.ID}" onclick="return false;"><i class="fa fa-fw fa-arrow-down" aria-hidden="true"></i></a>
											# ENDIF #
										</div>

										<script>
											jQuery(document).ready(function() {
												var delivery_item = new FaqItem({items.ID}, DeliveryItems);

												if (DeliveryItems.items_number > 1) {
													jQuery('#move-up-{items.ID}').on('click',function(){
														var li = jQuery(this).closest('li');
														li.insertBefore( li.prev() );
														DeliveryItems.change_reposition_pictures();
													});
													jQuery('#move-down-{items.ID}').on('click',function(){
														var li = jQuery(this).closest('li');
														li.insertAfter( li.next() );
														DeliveryItems.change_reposition_pictures();
													});
												}
											});
										</script>
									</li>
								# END items #
							</ul>
						</fieldset>
						# IF C_SEVERAL_ITEMS #
						<fieldset class="fieldset-submit" id="position-update-button">
							<button type="submit" name="submit" value="true" class="button submit">{@common.update.position}</button>
							<input type="hidden" name="token" value="{TOKEN}">
							<input type="hidden" name="tree" id="tree" value="">
						</fieldset>
						# ENDIF #
					</form>
				# ELSE #
					# IF NOT C_HIDE_NO_ITEM_MESSAGE #
						<div class="message-helper bgc notice align-center">
							{@common.no.item.now}
						</div>
					# ENDIF #
				# ENDIF #
			</div>
		</div>
	</div>
	<footer></footer>
</section>
