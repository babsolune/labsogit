<div id="card">
    <h2>{@delivery.title.card}</h2>
    <div class="pieces-display">
        # START pieces #
            <article id="{pieces.REWRITED_TITLE}" class="content piece-container">
                <header class="flex-between bgc-sub">
                    <div>
                        <div class="flex-between">
                            # IF IS_ADMIN #<a href="{pieces.U_EDIT}" class="offload moderator piece-edit" aria-label="{@common.edit}"><i class="fa fa-edit" aria-hidden="true"></i></a># ENDIF #
                            <h3>{pieces.TITLE}</h3>
                        </div>
                        # IF pieces.C_HAS_NUMBER #<span class="d-block">x{pieces.NUMBER} {@delivery.pieces}</span># ENDIF #
                    </div>
                    <div class="">
                        # IF pieces.C_HAS_THUMBNAIL #<img class="piece-thumbnail" src="{pieces.U_THUMBNAIL}" alt="{pieces.TITLE}" /># ENDIF #
                    </div>
                </header>
                <div class="cell-list cell-items">
                    <ul>
                        # START pieces.items #
                            <li>
                                <div class="flex-between">
                                    <span>
                                        # IF pieces.items.CODE #<span class="pinned bgc-sub">{pieces.items.CODE}</span># ENDIF #
                                        # IF pieces.items.NAME #{pieces.items.NAME}# ENDIF #
                                    </span>
                                    # IF pieces.items.PRICE #<div class="pinned bgc-alt">{pieces.items.PRICE}€</div># ENDIF #
                                </div>
                                <span class="d-block text-italic">{pieces.items.CONTENT}</span>
                            </li>
                        # END pieces.items #
                    </ul>
                </div>
            </article>
        # END pieces #
    </div>
</div>
<div class="spacer"></div>
<div id="trays">
    <h2>{@delivery.title.trays}</h2>
    <div class="">
        # START trays #
            <article id="{trays.REWRITED_TITLE}" class="content">
                <header class="flex-between bgc-sub">
                    <div>
                        <div class="flex-between">
                            # IF IS_ADMIN #<a href="{trays.U_EDIT}" class="offload moderator piece-edit" aria-label="{@common.edit}"><i class="fa fa-edit" aria-hidden="true"></i></a># ENDIF #
                            <h3>{trays.TITLE}</h3>
                        </div>
                        # IF trays.C_HAS_NUMBER #<span class="d-block">x{trays.NUMBER} {@delivery.pieces}</span># ENDIF #
                    </div>
                    <div class="">
                        # IF trays.C_HAS_THUMBNAIL #<img class="piece-thumbnail" src="{trays.U_THUMBNAIL}" alt="{trays.TITLE}" /># ENDIF #
                    </div>
                </header>
                <div class="cell-list cell-items">
                    <ul class="cell-flex cell-columns-3">
                        # START trays.items #
                            <li>
                                <div class="flex-between">
                                    <span>
                                        # IF trays.items.CODE #<span class="pinned bgc-sub">{trays.items.CODE}</span># ENDIF #
                                        # IF trays.items.NAME #{trays.items.NAME}# ENDIF #
                                    </span>
                                    # IF trays.items.PRICE #<div class="pinned bgc-alt">{trays.items.PRICE}€</div># ENDIF #
                                </div>
                                    <span class="d-block text-italic">{trays.items.CONTENT}</span>
                            </li>
                        # END trays.items #
                    </ul>
                </div>
            </article>
        # END trays #
    </div>
</div>
