# IF C_MONDAY #
    <div class="cell-body">
        <header class="cell-header align-center bgc-sub">
            <h5 class="cell-name">{@delivery.monday}</h5>
        </header>
        <div class="cell-list cell-items">
            <ul>
                # START monday #
                    <li>
                        <span class="pinned bgc-alt">{monday.TIME}</span>
                        <span class="text-strong">{monday.CITY}</span>
                        <span class="d-block">{monday.PLACE}</span>
                    </li>
                # END monday #
            </ul>
        </div>
    </div>
# ENDIF #
# IF C_TUESDAY #
    <div class="cell-body">
        <header class="cell-header align-center bgc-sub">
            <h5 class="cell-name">{@delivery.tuesday}</h5>
        </header>
        <div class="cell-list cell-items">
            <ul>
                # START tuesday #
                    <li>
                        <span class="pinned bgc-alt">{tuesday.TIME}</span>
                        <span class="text-strong">{tuesday.CITY}</span>
                        <span class="d-block">{tuesday.PLACE}</span>
                    </li>
                # END tuesday #
            </ul>
        </div>
    </div>
# ENDIF #
# IF C_WEDNESDAY #
    <div class="cell-body">
        <header class="cell-header align-center bgc-sub">
            <h5 class="cell-name">{@delivery.wednesday}</h5>
        </header>
        <div class="cell-list cell-items">
            <ul>
                # START wednesday #
                    <li>
                        <span class="pinned bgc-alt">{wednesday.TIME}</span>
                        <span class="text-strong">{wednesday.CITY}</span>
                        <span class="d-block">{wednesday.PLACE}</span>
                    </li>
                # END wednesday #
            </ul>
        </div>
    </div>
# ENDIF #
# IF C_THURSDAY #
    <div class="cell-body">
        <header class="cell-header align-center bgc-sub">
            <h5 class="cell-name">{@delivery.thursday}</h5>
        </header>
        <div class="cell-list cell-items">
            <ul>
                # START thursday #
                    <li>
                        <span class="pinned bgc-alt">{thursday.TIME}</span>
                        <span class="text-strong">{thursday.CITY}</span>
                        <span class="d-block">{thursday.PLACE}</span>
                    </li>
                # END thursday #
            </ul>
        </div>
    </div>
# ENDIF #
# IF C_FRIDAY #
    <div class="cell-body">
        <header class="cell-header align-center bgc-sub">
            <h5 class="cell-name">{@delivery.friday}</h5>
        </header>
        <div class="cell-list cell-items">
            <ul>
                # START friday #
                    <li>
                        <span class="pinned bgc-alt">{friday.TIME}</span>
                        <span class="text-strong">{friday.CITY}</span>
                        <span class="d-block">{friday.PLACE}</span>
                    </li>
                # END friday #
            </ul>
        </div>
    </div>
# ENDIF #
# IF C_SATURDAY #
    <div class="cell-body">
        <header class="cell-header align-center bgc-sub">
            <h5 class="cell-name">{@delivery.saturday}</h5>
        </header>
        <div class="cell-list cell-items">
            <ul>
                # START saturday #
                    <li>
                        <span class="pinned bgc-alt">{saturday.TIME}</span>
                        <span class="text-strong">{saturday.CITY}</span>
                        <span class="d-block">{saturday.PLACE}</span>
                    </li>
                # END saturday #
            </ul>
        </div>
    </div>
# ENDIF #
# IF C_SUNDAY #
    <div class="cell-body">
        <header class="cell-header align-center bgc-sub">
            <h5 class="cell-name">{@delivery.sunday}</h5>
        </header>
        <div class="cell-list cell-items">
            <ul>
                # START sunday #
                    <li>
                        <span class="pinned bgc-alt">{sunday.TIME}</span>
                        <span class="text-strong">{sunday.CITY}</span>
                        <span class="d-block">{sunday.PLACE}</span>
                    </li>
                # END sunday #
            </ul>
        </div>
    </div>
# ENDIF #
