<div id="sticky-menu" class="sticky-menu no-bgc">
    <nav id="menu-card" class="cssmenu cssmenu-horizontal cssmenu-with-submenu">
        <ul class="level-0">
            <li class="hidden-large-screens"><a class="cssmenu-title" href="#command"><span>{@delivery.menu.command}</span></a></li>
            <li class="has-sub">
                <a class="cssmenu-title" href="#card"><span>{@delivery.menu.card}</span></a>
                <ul class="level-1">
                    # START pieces #
                        <li><a class="cssmenu-title" href="\#{pieces.REWRITED_TITLE}"><span>{pieces.TITLE}</span></a></li>
                    # END pieces #
                </ul>
            </li>
            <li class="has-sub">
                <a class="cssmenu-title" href="#trays"><span>{@delivery.menu.tray}</span></a>
                <ul class="level-1">
                    # START trays #
                        <li><a class="cssmenu-title" href="\#{trays.REWRITED_TITLE}"><span>{trays.TITLE}</span></a></li>
                    # END trays #
                </ul>
            </li>
            <li class="hidden-large-screens"><a class="cssmenu-title" href="#delivery"><span>{@delivery.menu.delivery}</span></a></li>
        </ul>
    </nav>
    <script>jQuery("#menu-card").menumaker({ title: "{@delivery.menu}", format: "multitoggle", breakpoint: 768 });</script>
</div>
<script>
    document.addEventListener('DOMContentLoaded', (event) => {
    const stickyDiv = document.getElementById('sticky-menu');

    function checkStickyDivPosition() {
        const rect = stickyDiv.getBoundingClientRect();
        if (rect.top === 0) {
            stickyDiv.classList.add('is-sticky');
            console.log('Sticky div is at the top of the viewport');
        } else {
            stickyDiv.classList.remove('is-sticky');
            console.log('Sticky div is not at the top of the viewport');
        }
    }

    window.addEventListener('scroll', checkStickyDivPosition);
    checkStickyDivPosition(); // Initial check
});
</script>