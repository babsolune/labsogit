<script>
    // Sortable
	jQuery(document).ready(function() {
		if (jQuery("#input_fields_${escape(ID)}")[0]) {
			jQuery("#input_fields_${escape(ID)}").sortable({
				handle: '.sortable-selector',
				placeholder: '<div class="dropzone">' + ${escapejs(@common.drop.here)} + '</div>',
                afterMove: function(a, b, c) {
                },
				onDrop: function ($item, container, _super, event) {
					$item.removeClass(container.group.options.draggedClass).removeAttr("style");
					$("body").removeClass(container.group.options.bodyClass);
                    change_ids();
				}
			});
		}
	});

    function change_ids()
    {
        let li = jQuery("#input_fields_${escape(ID)} li");
        li.each(function() {
            jQuery(this).attr('id', '${escape(ID)}_' + jQuery(this).index());
            jQuery(this).find('.item-hour').attr('id', 'field_hour_${escape(ID)}_' + jQuery(this).index()).attr('name', 'field_hour_${escape(ID)}_' + jQuery(this).index());
            jQuery(this).find('.item-minutes').attr('id', 'field_minutes_${escape(ID)}_' + jQuery(this).index()).attr('name', 'field_minutes_${escape(ID)}_' + jQuery(this).index());
            jQuery(this).find('.move-up').attr('id', 'move-up-${escape(ID)}_' + jQuery(this).index());
            jQuery(this).find('.move-down').attr('id', 'move-down-${escape(ID)}_' + jQuery(this).index());
            jQuery(this).find('.item-delete').attr('href', 'javascript:DeliveryFormFieldDay.delete_field(' + jQuery(this).index() + ')');
            jQuery(this).find('.item-city').attr('id', 'field_city_${escape(ID)}_' + jQuery(this).index()).attr('name', 'field_city_${escape(ID)}_' + jQuery(this).index());
            jQuery(this).find('.item-place').attr('id', 'field_place_${escape(ID)}_' + jQuery(this).index()).attr('name', 'field_place_${escape(ID)}_' + jQuery(this).index());
        })
    }

    // Mange more items
	var DeliveryFormFieldDay = function(){
		this.integer = {FIELDS_NUMBER};
		this.id_input = ${escapejs(ID)};
		this.max_input = {MAX_INPUT};
	};

	DeliveryFormFieldDay.prototype = {
		add_field : function (field_id, id_input) {
			if (this.integer <= this.max_input) {
				var id = id_input + '_' + this.integer;
console.log(field_id);
				jQuery('<li/>', {'id' : id, class : 'day-item'}).appendTo('#' + field_id);

                jQuery('<div/> ', {'class' : 'grouped-inputs'}).appendTo('#' + id);

				jQuery('<span/>', {'class' : 'sortable-selector grouped-element', 'aria-label' : ${escapejs(@common.move)}}).html('&nbsp;').appendTo('#' + id + ' .grouped-inputs' );

                jQuery('<input/> ', {'type' : 'number', min : '0', max : '23', id : 'field_hour_' + id, class : 'grouped-element item-hour input-time', value : '18', name : 'field_hour_' + id}).appendTo('#' + id + ' .grouped-inputs');
				jQuery('#' + id).append(' ');

                jQuery('<input/> ', {'type' : 'number', min : '0', max : '55', step : '5', id : 'field_minutes_' + id, class : 'grouped-element item-minutes input-time', value : '00', 'step' : '05', name : 'field_minutes_' + id}).appendTo('#' + id + ' .grouped-inputs');
				jQuery('#' + id).append(' ');

                jQuery('<a/> ', {'href' : 'javascript:DeliveryFormFieldDay.delete_field("'+ id_input +'", '+ this.integer +');', class : 'grouped-element item-delete bgc-full error', 'data-confirmation' : 'delete-element', 'aria-label' : ${escapejs(@common.delete)}}).html('<i class="fa fa-trash-alt" aria-hidden="true"></i>').appendTo('#' + id + ' .grouped-inputs');

				jQuery('<input/> ', {'type' : 'text', id : 'field_city_' + id, class : 'item-city flex-wide', name : 'field_city_' + id, placeholder : ${escapejs(@delivery.day.city)}}).appendTo('#' + id);
				jQuery('#' + id).append(' ');

                jQuery('<input/> ', {'type' : 'text', id : 'field_place_' + id, class : 'item-place flex-wide', name : 'field_place_' + id, placeholder : ${escapejs(@delivery.day.place)}}).appendTo('#' + id);
				jQuery('#' + id).append(' ');

				this.integer++;
			}
			if (this.integer == this.max_input) {
				jQuery('#add-' + id_input).hide();
			}
		},
		delete_field : function (input, id) {
			var id = input + '_' + id;
			jQuery('#' + id).remove();
			this.integer--;
			jQuery('#add-' + input).show();
		}
	};

	var DeliveryFormFieldDay = new DeliveryFormFieldDay();
</script>

<ul id="input_fields_${escape(ID)}" class="sortable-block">
	# START fieldelements #
		<li id="${escape(ID)}_{fieldelements.ID}" class="piece-item">
            <div class="grouped-inputs">
                <span class="sortable-selector grouped-element" aria-label="{@common.move}">&nbsp;</span>
                <a href="#" class="move-up grouped-element" aria-label="{@common.move.up}" id="move-up-${escape(ID)}_{fieldelements.ID}" onclick="return false;"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
                <a href="#" class="move-down grouped-element" aria-label="{@common.move.down}" id="move-down-${escape(ID)}_{fieldelements.ID}" onclick="return false;"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                <input id="field_hour_${escape(ID)}_{fieldelements.ID}" min="0" max="23" class="grouped-element item-hour input-time" type="number" name="field_hour_${escape(ID)}_{fieldelements.ID}" pattern="\d{2}" value="{fieldelements.HOUR}"/>
                <input id="field_minutes_${escape(ID)}_{fieldelements.ID}" min="0" max="55" step="5" class="grouped-element item-minutes input-time" type="number" name="field_minutes_${escape(ID)}_{fieldelements.ID}" pattern="\d{2}" value="{fieldelements.MINUTES}"/>
                <a class="grouped-element item-delete bgc-full error" href="javascript:DeliveryFormFieldDay.delete_field({fieldelements.ID});" data-confirmation="delete-element" aria-label="{@common.delete}"><i class="fa fa-trash-alt" aria-hidden="true"></i></a>
            </div>
            <input id="field_city_${escape(ID)}_{fieldelements.ID}" class="grouped-element item-city" type="text" name="field_city_${escape(ID)}_{fieldelements.ID}" value="{fieldelements.CITY}" placeholder="{@delivery.day.city}"/>
            <input id="field_place_${escape(ID)}_{fieldelements.ID}" class="grouped-element item-place" type="text" name="field_place_${escape(ID)}_{fieldelements.ID}" value="{fieldelements.PLACE}" placeholder="{@delivery.day.place}"/>
            <script>
                jQuery(document).ready(function() {
                    if (jQuery('#field_hour_${escape(ID)}_{fieldelements.ID}').val() == '')
                        jQuery('#field_hour_${escape(ID)}_{fieldelements.ID}').val('18');
                    if (jQuery('#field_minutes_${escape(ID)}_{fieldelements.ID}').val() == '')
                        jQuery('#field_minutes_${escape(ID)}_{fieldelements.ID}').val('00');
                    jQuery("#move-up-${escape(ID)}_{fieldelements.ID}").on('click',function(){
                        var li = jQuery(this).closest('li');
                        li.insertBefore( li.prev() );
                        change_ids();
                    });

                    jQuery("#move-down-${escape(ID)}_{fieldelements.ID}").on('click',function(){
                        var li = jQuery(this).closest('li');
                        li.insertAfter( li.next() );
                        change_ids();
                    });
                });
                // Function to format number to two digits
                function formatToTwoDigits(input) {
                    let value = input.value;
                    value = value.replace(/\D/g, '');
                    // Ensure the value is exactly two digits
                    if (value.length > 2) {
                        value = value.slice(0, 2);
                    } else if (value.length < 2 && value.length > 0) {
                        value = '0' + value;
                    }

                    // Update the input value
                    event.target.value = value;
                }

                // Add event listeners to all number inputs
                document.querySelectorAll('input[type="number"]').forEach(input => {
                    input.addEventListener('change', () => formatToTwoDigits(input));
                });
            </script>
        </li>
	# END fieldelements #
</ul>
<a href="javascript:DeliveryFormFieldDay.add_field('input_fields_${escape(ID)}', '${escape(ID)}');" id="add-${escape(ID)}" class="add-more-values" aria-label="{@common.add}"><i class="far fa-lg fa-plus-square" aria-hidden="true"></i></a>

