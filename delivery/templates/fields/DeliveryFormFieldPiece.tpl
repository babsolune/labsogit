<script>
    // Sortable
	jQuery(document).ready(function() {
		if (jQuery("#input_fields_${escape(ID)}")[0]) {
			jQuery("#input_fields_${escape(ID)}").sortable({
				handle: '.sortable-selector',
				placeholder: '<div class="dropzone">' + ${escapejs(@common.drop.here)} + '</div>',
                afterMove: function(a, b, c) {
                },
				onDrop: function ($item, container, _super, event) {
					$item.removeClass(container.group.options.draggedClass).removeAttr("style");
					$("body").removeClass(container.group.options.bodyClass);
                    change_ids();
				}
			});
		}
	});

    function change_ids()
    {
        let li = jQuery("#input_fields_${escape(ID)} li");
        li.each(function() {
            jQuery(this).attr('id', '${escape(ID)}_' + jQuery(this).index());
            jQuery(this).find('.item-code').attr('id', 'field_code_${escape(ID)}_' + jQuery(this).index()).attr('name', 'field_code_${escape(ID)}_' + jQuery(this).index());
            jQuery(this).find('.move-up').attr('id', 'move-up-${escape(ID)}_' + jQuery(this).index());
            jQuery(this).find('.move-down').attr('id', 'move-down-${escape(ID)}_' + jQuery(this).index());
            jQuery(this).find('.item-delete').attr('href', 'javascript:DeliveryFormFieldPiece.delete_field(' + jQuery(this).index() + ')');
            jQuery(this).find('.item-name').attr('id', 'field_name_${escape(ID)}_' + jQuery(this).index()).attr('name', 'field_name_${escape(ID)}_' + jQuery(this).index());
            jQuery(this).find('.item-price').attr('id', 'field_price_${escape(ID)}_' + jQuery(this).index()).attr('name', 'field_price_${escape(ID)}_' + jQuery(this).index());
            jQuery(this).find('.item-content').attr('id', 'field_content_${escape(ID)}_' + jQuery(this).index()).attr('name', 'field_content_${escape(ID)}_' + jQuery(this).index());
        })
    }

    // Mange more items
	var DeliveryFormFieldPiece = function(){
		this.integer = {FIELDS_NUMBER};
		this.id_input = ${escapejs(ID)};
		this.max_input = {MAX_INPUT};
	};

	DeliveryFormFieldPiece.prototype = {
		add_field : function () {
			if (this.integer <= this.max_input) {
				var id = this.id_input + '_' + this.integer;

				jQuery('<li/>', {'id' : id, class : 'piece-item'}).appendTo('#input_fields_' + this.id_input);

                jQuery('<div/> ', {class : 'grouped-inputs grouped-header'}).appendTo('#' + id);

				jQuery('<span/>', {class : 'sortable-selector grouped-element', 'aria-label' : ${escapejs(@common.move)}}).html('&nbsp;').appendTo('#' + id + ' .grouped-header' );

                jQuery('<input/> ', {type : 'text', id : 'field_code_' + id, class : 'grouped-element item-code flex-wide', name : 'field_code_' + id, placeholder : ${escapejs(@delivery.piece.code)}}).appendTo('#' + id + ' .grouped-header');
				jQuery('#' + id).append(' ');

                jQuery('<input/> ', {type : 'text', id : 'field_price_' + id, class : 'grouped-element item-price flex-wide', name : 'field_price_' + id, placeholder : ${escapejs(@delivery.piece.price)}}).appendTo('#' + id + ' .grouped-header');
				jQuery('#' + id).append(' ');

				jQuery('<a/> ', {href : 'javascript:DeliveryFormFieldPiece.delete_field('+ this.integer +');', class : 'grouped-element item-delete bgc-full error', 'data-confirmation' : 'delete-element', 'aria-label' : ${escapejs(@common.delete)}}).html('<i class="fa fa-trash-alt" aria-hidden="true"></i>').appendTo('#' + id + ' .grouped-header');

				jQuery('<div/> ', {class : 'grouped-inputs grouped-body'}).appendTo('#' + id);

				jQuery('<input/> ', {type : 'text', id : 'field_name_' + id, class : 'grouped-element item-name', name : 'field_name_' + id, placeholder : ${escapejs(@delivery.piece.name)}}).appendTo('#' + id + ' .grouped-body');
				jQuery('#' + id).append(' ');

				jQuery('<textarea/> ', {id : 'field_content_' + id, name : 'field_content_' + id, class : 'grouped-area item_content', placeholder : ${escapejs(@delivery.piece.content)}}).appendTo('#' + id);
				jQuery('#' + id).append(' ');

				this.integer++;
			}
			if (this.integer == this.max_input) {
				jQuery('#add-' + this.id_input).hide();
			}
		},
		delete_field : function (id) {
			var id = this.id_input + '_' + id;
			jQuery('#' + id).remove();
			this.integer--;
			jQuery('#add-' + this.id_input).show();
		}
	};

	var DeliveryFormFieldPiece = new DeliveryFormFieldPiece();
</script>

<ul id="input_fields_${escape(ID)}" class="sortable-block">
	# START fieldelements #
		<li id="${escape(ID)}_{fieldelements.ID}" class="piece-item">
            <div class="grouped-inputs">
                <span class="sortable-selector grouped-element" aria-label="{@common.move}">&nbsp;</span>
                <a href="#" class="move-up grouped-element" aria-label="{@common.move.up}" id="move-up-${escape(ID)}_{fieldelements.ID}" onclick="return false;"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
                <a href="#" class="move-down grouped-element" aria-label="{@common.move.down}" id="move-down-${escape(ID)}_{fieldelements.ID}" onclick="return false;"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
                <input id="field_code_${escape(ID)}_{fieldelements.ID}" class="grouped-element item-code flex-wide" type="text" name="field_code_${escape(ID)}_{fieldelements.ID}" value="{fieldelements.CODE}" placeholder="{@delivery.piece.code}"/>
                <input id="field_price_${escape(ID)}_{fieldelements.ID}" class="grouped-element item-price" type="text" name="field_price_${escape(ID)}_{fieldelements.ID}" value="{fieldelements.PRICE}" placeholder="{@delivery.piece.price}"/>
                <a class="grouped-element item-delete bgc-full error" href="javascript:DeliveryFormFieldPiece.delete_field({fieldelements.ID});" data-confirmation="delete-element" aria-label="{@common.delete}"><i class="fa fa-trash-alt" aria-hidden="true"></i></a>
            </div>
            <div class="grouped-inputs">
                <input id="field_name_${escape(ID)}_{fieldelements.ID}" class="grouped-element item-name" type="text" name="field_name_${escape(ID)}_{fieldelements.ID}" value="{fieldelements.NAME}" placeholder="{@delivery.piece.name}"/>
            </div>
			<textarea class="grouped-area item-content" name="field_content_${escape(ID)}_{fieldelements.ID}" id="field_content_${escape(ID)}_{fieldelements.ID}" placeholder="{@delivery.piece.content}">{fieldelements.CONTENT}</textarea>

            <script>
                jQuery(document).ready(function() {
                    jQuery("#move-up-${escape(ID)}_{fieldelements.ID}").on('click',function(){
                        var li = jQuery(this).closest('li');
                        li.insertBefore( li.prev() );
                        change_ids();
                    });

                    jQuery("#move-down-${escape(ID)}_{fieldelements.ID}").on('click',function(){
                        var li = jQuery(this).closest('li');
                        li.insertAfter( li.next() );
                        change_ids();
                    });
                });
            </script>
        </li>
	# END fieldelements #
</ul>
<a href="javascript:DeliveryFormFieldPiece.add_field();" id="add-${escape(ID)}" class="add-more-values" aria-label="{@common.add}"><i class="far fa-lg fa-plus-square" aria-hidden="true"></i></a>

