<!DOCTYPE html>
<html lang="{@common.xml.lang}">
	<head>
		<title>{PAGE_TITLE}</title>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{PATH_TO_ROOT}/templates/{THEME}/theme/@import.css" type="text/css" media="screen, print" />
        <link rel="stylesheet" href="{PATH_TO_ROOT}/delivery/templates/delivery.css" type="text/css" media="screen, print" />
        <script src="{PATH_TO_ROOT}/delivery/templates/js/html2canvas.js"></script>
	</head>
	<body id="print">
        <div class="flex-between print-buttons small">
            <div>
                <div class="content">
                    <a href="../" class="content pinned bgc-full link-color">{@common.home}</a>
                    <a id="download" download class="content pinned bgc-full success hidden">Télécharger</a>
                </div>
                <div class="content">
                    <a class="content pinned bgc-full link-color" onclick="capturePrint('card');">{@delivery.print.card}</a>
                    <a class="content pinned bgc-full link-color" onclick="capturePrint('flyer');">{@delivery.print.flyer}</a>
                </div>
            </div>
            <div class="align-right">
                <div class="content">
                    <a class="content pinned bgc-full link-color" onclick="capturePrint('logo-light');">{@delivery.print.logo.light}</a>
                    <a class="content pinned bgc-full link-color" onclick="capturePrint('logo-dark');">{@delivery.print.logo.dark}</a>
                </div>
                <div class="content">
                    <a class="content pinned bgc-full link-color" onclick="capturePrint('logo-light-rounded');">{@delivery.print.logo.rounded.light}</a>
                    <a class="content pinned bgc-full link-color" onclick="capturePrint('logo-dark-rounded');">{@delivery.print.logo.rounded.dark}</a>
                </div>
            </div>
        </div>
        <div id="canvas"></div>
        <div id="global">
            <div id="global-container">
                <div id="main">
                    <div id="main-content">
                        <section>
                            <div class="sub-section">
                                <div class="content-container">
                                    <div id="card" class="bgc-main">
                                        <div class="content-wrapper">
                                            # INCLUDE CARD #
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sub-section">
                                <div class="content-container">
                                    <div id="flyer" class="bgc-main sookie-logo">
                                        <div class="content-wrapper">
                                            <img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/logo-sookie.webp" alt="logo">
                                            <div class="cell-flex cell-columns-2">
                                                # INCLUDE COMMAND #
                                            </div>
                                            <div class="flex-between print-qrcode">
                                                <h2 class="flex-wide">{@delivery.see.qrcode}</h2>
                                                <div class="flex-wide">
                                                    <img class="" src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/qrcode-sample.webp" alt="card">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sub-section">
                                <div class="content-container">
                                    <div id="logo-light" class="">
                                        <img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/sookie-light.webp" alt="logo">
                                    </div>
                                </div>
                            </div>
                            <div class="sub-section">
                                <div class="content-container">
                                    <div id="logo-dark" class="">
                                        <img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/sookie-dark.webp" alt="logo">
                                    </div>
                                </div>
                            </div>
                            <div class="sub-section bgc-main">
                                <div class="content-container">
                                    <div id="logo-light-rounded" class="">
                                        <img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/sookie-light-rounded.webp" alt="logo">
                                    </div>
                                </div>
                            </div>
                            <div class="sub-section bgc-main">
                                <div class="content-container">
                                    <div id="logo-dark-rounded" class="">
                                        <img src="{PATH_TO_ROOT}/templates/{THEME}/theme/images/sookie-dark-rounded.webp" alt="logo">
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        <script>
            let currentCanvas = null;
            let canvasType = null;
            let downloadBtn = document.getElementById('download');
            function capturePrint(el) {
                html2canvas(document.querySelector("#" + el),{backgroundColor: null}).then(canvas => {
                    const canvasContainer = document.getElementById('canvas');
                    canvasContainer.innerHTML = ''; // Clear previous canvas
                    canvasContainer.appendChild(canvas);
                    currentCanvas = canvas;
                    canvasType = el;
                    downloadBtn.classList.remove('hidden');
                });
            }
            document.addEventListener('DOMContentLoaded', function() {
                // Add event listener to the download button
                downloadBtn.addEventListener('click', function() {
                    if (currentCanvas) {
                        // Convert the canvas content to a data URL
                        const dataURL = currentCanvas.toDataURL('image/png');

                        // Create an anchor element
                        const link = document.createElement('a');
                        link.href = dataURL;
                        switch (canvasType) {
                            case 'card' :
                                link.download = 'card.png';
                            break;
                            case 'flyer' :
                                link.download = 'flyer.png';
                            break;
                            case 'logo-light' :
                                link.download = 'logo-light.png';
                            break;
                            case 'logo-dark' :
                                link.download = 'logo-dark.png';
                            break;
                            case 'logo-light-rounded' :
                                link.download = 'logo-light-rounded.png';
                            break;
                            case 'logo-dark-rounded' :
                                link.download = 'logo-dark-rounded.png';
                            break;
                        }

                        link.click();
                    }
                });
            });
        </script>
	</body>
</html>