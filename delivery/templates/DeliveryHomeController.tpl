# INCLUDE MESSAGE_HELPER #

<section id="module-sookie" class="several-items">
    <a href="#global" id="scroller" aria-label="{@common.go.to.content}">
        <i class="fa fa-2x fa-fw fa-computer-mouse" aria-hidden="true"></i><i class="fa fa-fw fa-arrows-up-down fa-bounce" aria-hidden="true"></i>
    </a>
    <a class="content bgc-sub" href="https://www.facebook.com/profile.php?id=61572716894457" target="_blank" rel="noopener noreferrer" id="facebook" aria-label="{@delivery.facebook}">
        <i class="fab fa-2x fa-fw fa-square-facebook" aria-hidden="true"></i>
    </a>
    <header class="section-header">
        <h1 id="module-home">
            {@delivery.module.title}
        </h1>
    </header>
    # INCLUDE MENU #
	<div id="command" class="sub-section hidden-large-screens">
		<div class="content-container align-center">
            # INCLUDE COMMAND #
		</div>
	</div>
	<div id="menu" class="sub-section">
		<div class="content-container">
            # INCLUDE CARD #
		</div>
	</div>
	<div id="delivery" class="sub-section hidden-large-screens">
		<div class="content-container">
            <h2>{@delivery.title.delivery}</h2>
            # INCLUDE DAYS #
		</div>
	</div>
	<footer></footer>
</section>

