<div id="module-mini-delivery" class="cell-mini cell-tile# IF C_VERTICAL_BLOCK # cell-mini-vertical# ENDIF ## IF C_HIDDEN_WITH_SMALL_SCREENS # hidden-small-screens# ENDIF #">
    <div class="cell">
        <div class="cell-header">
            <h6 class="cell-name">
                {@delivery.mini.delivery}
            </h6>
        </div>
        # INCLUDE DAYS #
    </div>
</div>
