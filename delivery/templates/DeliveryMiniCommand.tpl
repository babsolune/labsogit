<div id="module-mini-delivery-command" class="cell-mini cell-tile# IF C_VERTICAL_BLOCK # cell-mini-vertical# ENDIF ## IF C_HIDDEN_WITH_SMALL_SCREENS # hidden-small-screens# ENDIF #">
    <div class="cell">
        <div class="cell-header">
            <h6 class="cell-name">
                {@delivery.mini.command}
            </h6>
        </div>
        <div class="cell-body">
            <div class="cell-content align-center">
                # INCLUDE COMMAND #
            </div>
        </div>
    </div>
</div>
