<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryFormFieldPiece extends AbstractFormField
{
	private $max_input = 200;

	public function __construct($id, $label, array $value = [], array $field_options = [], array $constraints = [])
	{
		parent::__construct($id, $label, $value, $field_options, $constraints);
	}

	function display()
	{
		$template = $this->get_template_to_use();

		$view = new FileTemplate('delivery/fields/DeliveryFormFieldPiece.tpl');
		$view->add_lang(LangLoader::get_all_langs('delivery'));

		$view->put_all([
			'NAME'       => $this->get_html_id(),
			'ID'         => $this->get_html_id(),
			'C_DISABLED' => $this->is_disabled()
		]);

		$this->assign_common_template_variables($template);

		$i = 0;
		foreach ($this->get_value() as $id => $options)
		{
			$view->assign_block_vars('fieldelements', [
				'ID'      => $id,
				'CODE'    => $options['code'],
				'NAME'    => $options['name'],
				'PRICE'   => $options['price'],
				'CONTENT' => $options['content'],
			]);
			$i++;
		}

		if ($i == 0)
		{
			$view->assign_block_vars('fieldelements', [
				'ID'      => 0,
				'CODE'    => '',
				'NAME'    => '',
				'PRICE'   => '',
				'CONTENT' => ''
			]);
		}

		$view->put_all([
			'MAX_INPUT'     => $this->max_input,
			'FIELDS_NUMBER' => $i == 0 ? 1 : $i
		]);

		$template->assign_block_vars('fieldelements', [
			'ELEMENT' => $view->render()
		]);

		return $template;
	}

	public function retrieve_value()
	{
		$request = AppContext::get_request();
		$values = [];
		for ($i = 0; $i < $this->max_input; $i++)
		{
            $field_name_id = 'field_name_' . $this->get_html_id() . '_' . $i;
            $field_code_id = 'field_code_' . $this->get_html_id() . '_' . $i;
			if ($request->has_postparameter($field_code_id) || $request->has_postparameter($field_name_id))
			{
				$field_name = $request->get_poststring($field_name_id);
				$field_code = $request->get_poststring($field_code_id);
				$field_price_id = 'field_price_' . $this->get_html_id() . '_' . $i;
				$field_price = $request->get_poststring($field_price_id);
                $field_content_id = 'field_content_' . $this->get_html_id() . '_' . $i;
				$field_content = $request->get_poststring($field_content_id);

				if (!empty($field_code) || !empty($field_name))
					$values[] = [
                        'code'    => $field_code,
                        'name'    => $field_name,
                        'price'   => $field_price,
                        'content' => $field_content,
                    ];
			}
		}
		$this->set_value($values);
	}

	protected function compute_options(array &$field_options)
	{
		foreach($field_options as $attribute => $value)
		{
			$attribute = TextHelper::strtolower($attribute);
			switch ($attribute)
			{
			case 'max_input':
				$this->max_input = $value;
				unset($field_options['max_input']);
				break;
			}
		}
		parent::compute_options($field_options);
	}

	protected function get_default_template()
	{
		return new FileTemplate('framework/builder/form/FormField.tpl');
	}
}
?>
