<?php
/**
 * @copyright   &copy; 2005-2025 PHPBoost
 * @license     https://www.gnu.org/licenses/gpl-3.0.html GNU/GPL-3.0
 * @author      Sebastien LARTIGUE <babsolune@phpboost.com>
 * @version     PHPBoost 6.0 - last update: 2025 02 14
 * @since       PHPBoost 6.0 - 2025 02 14
 */

class DeliveryFormFieldDay extends AbstractFormField
{
	private $max_input = 200;

	public function __construct($id, $label, array $value = [], array $field_options = [], array $constraints = [])
	{
		parent::__construct($id, $label, $value, $field_options, $constraints);
	}

	function display()
	{
		$template = $this->get_template_to_use();

		$view = new FileTemplate('delivery/fields/DeliveryFormFieldDay.tpl');
		$view->add_lang(LangLoader::get_all_langs('delivery'));

		$view->put_all([
			'NAME'       => $this->get_html_id(),
			'ID'         => $this->get_html_id(),
			'C_DISABLED' => $this->is_disabled()
		]);

		$this->assign_common_template_variables($template);

		$i = 0;
		foreach ($this->get_value() as $id => $options)
		{
			$view->assign_block_vars('fieldelements', [
				'ID'      => $id,
				'HOUR'    => $options['hour'],
				'MINUTES' => $options['minutes'],
				'CITY'    => $options['city'],
				'PLACE'   => $options['place'],
			]);
			$i++;
		}

		if ($i == 0)
		{
			$view->assign_block_vars('fieldelements', [
				'ID'      => 0,
				'HOUR'    => '',
				'MINUTES' => '',
				'CITY'    => '',
				'PLACE'   => '',
			]);
		}

		$view->put_all([
			'MAX_INPUT'     => $this->max_input,
			'FIELDS_NUMBER' => $i == 0 ? 1 : $i
		]);

		$template->assign_block_vars('fieldelements', [
			'ELEMENT' => $view->render()
		]);

		return $template;
	}

	public function retrieve_value()
	{
		$request = AppContext::get_request();
		$values = [];
		for ($i = 0; $i < $this->max_input; $i++)
		{
            $field_city_id = 'field_city_' . $this->get_html_id() . '_' . $i;
			if ($request->has_postparameter($field_city_id))
			{
				$field_city = $request->get_poststring($field_city_id);
                $field_hour_id = 'field_hour_' . $this->get_html_id() . '_' . $i;
				$field_hour = $request->get_poststring($field_hour_id);
				$field_minutes_id = 'field_minutes_' . $this->get_html_id() . '_' . $i;
				$field_minutes = $request->get_poststring($field_minutes_id);
                $field_place_id = 'field_place_' . $this->get_html_id() . '_' . $i;
				$field_place = $request->get_poststring($field_place_id);

				if (!empty($field_city))
					$values[] = [
                        'hour'    => $field_hour,
                        'minutes' => $field_minutes,
                        'city'    => $field_city,
                        'place'   => $field_place,
                    ];
			}
		}
		$this->set_value($values);
	}

	protected function compute_options(array &$field_options)
	{
		foreach($field_options as $attribute => $value)
		{
			$attribute = TextHelper::strtolower($attribute);
			switch ($attribute)
			{
			case 'max_input':
				$this->max_input = $value;
				unset($field_options['max_input']);
				break;
			}
		}
		parent::compute_options($field_options);
	}

	protected function get_default_template()
	{
		return new FileTemplate('framework/builder/form/FormField.tpl');
	}
}
?>
