<?php

// Players list
$players = [
    'Player 1', 'Player 2', 'Player 3', 'Player 4', 'Player 5', 'Player 6', 'Player 7', 'Player 8', 'Player 9', 'Player 10',
    'Player 11', 'Player 12', 'Player 13', 'Player 14', 'Player 15', 'Player 16', 'Player 17', 'Player 18', 'Player 19'
];

// Randomize players list
shuffle($players);

// Fonction pour former les équipes
function build_teams($players) {
    $teams = [];

    $players_number = count($players);
    $teams_of_2 = floor($players_number / 2);
    $teams_of_2_odd = $teams_of_2 % 2 == 0;
    $teams_of_3 = floor($players_number / 3);
    $teams_of_3_odd = $teams_of_3 % 2 == 0;

    // As long as there are players in the list
    while (!empty($players)) {
        // If number of teams of 2 possible is even and number of teams of 3 possible is even
        // or if number of teams of 2 possible is even and number of teams of 3 possible is odd
        if (($teams_of_2_odd && $teams_of_3_odd) || ($teams_of_2_odd && !$teams_of_3_odd))
        {
            if (count($players) <= 3) {
                $team = array_splice($players, 0, 3);
            } else {
                $team = array_splice($players, 0, 2);
            }
        }
        // If number of teams of 2 possible is odd and number of teams of 3 possible is even
        // or if number of teams of 2 possible is odd and number of teams of 3 possible is odd
        elseif ((!$teams_of_2_odd && $teams_of_3_odd) || (!$teams_of_2_odd && !$teams_of_3_odd))
        {
            if (count($players) <= 9) {
                $team = array_splice($players, 0, 3);
            } else {
                $team = array_splice($players, 0, 2);
            }
        }

        // Add team to the list
        $teams[] = $team;
    }

    return $teams;
}

// Display the teams list
foreach (build_teams($players) as $index => $equipe) {
    echo "Team " . ($index + 1) . ": [ " . implode(', ', $equipe) . " ]<br>";
}

?>
